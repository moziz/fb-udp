// FB UDP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <csignal>
#include <iostream>
#include <string>

#include "NetworkService.h"

constexpr unsigned short listenOnPort = 3000;
constexpr long echoTimeout = 1;

bool serve = false;

void handleSignal(int sig) {
  std::cout << "Stopping server..." << std::endl;
  serve = true;
}

int main(int argc, char** argv) {
  signal(SIGINT, handleSignal);

  if (NetworkService::initSystem()) {
    std::cerr << "NetworkService initialization failed" << std::endl;
    return 1;
  }

  std::cout << "Starting the UDP echo server on port " << listenOnPort << "..." << std::endl;

  NetworkService networkService;
  if (networkService.listen(listenOnPort)) {
    std::cerr << "Server startup failed" << std::endl;
    WSACleanup();
    return 1;
  }

  std::cout << "Listening on port " << listenOnPort << std::endl;

  while (!serve) {
    NetworkService::EchoStatus status = networkService.echoSynchronously(echoTimeout);

    if (status == NetworkService::EchoStatus::error) {
      std::cout << "An error occured. Terminating server." << std::endl;
      serve = false;
    }
  }

  std::cout << "Closing the server..." << std::endl;

  networkService.close();
  NetworkService::cleanSystem();

  return 0;
}
