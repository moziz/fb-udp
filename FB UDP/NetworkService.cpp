#include "stdafx.h"
#include "NetworkService.h"

#include <iostream>

NetworkService::NetworkService():
  mSocket(INVALID_SOCKET),
  mInAddress(),
  mIsListening(false)
{}

NetworkService::~NetworkService() {
  close();
}

bool NetworkService::listen(unsigned short port) {
  mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  
  if (mSocket == INVALID_SOCKET) {
    std::cerr << "Socket creation failed. Error: " << WSAGetLastError() << std::endl;
    return true;
  }

  // Setup inbound address
  mInAddress.sin_family = AF_INET;
  mInAddress.sin_port = htons(port);
  mInAddress.sin_addr.s_addr = htonl(INADDR_ANY);

  // Bind socket to a port
  if (bind(mSocket, (SOCKADDR*)&mInAddress, sizeof(mInAddress)) == SOCKET_ERROR) {
    std::cerr << "Socket binding failed. Error: " << WSAGetLastError() << std::endl;
    return true;
  }
  
  mIsListening = true;
  return false;
}

void NetworkService::close() {
  if (!mIsListening) {
    return;
  }

  int closeResult = closesocket(mSocket);

  if (closeResult == SOCKET_ERROR) {
    std::cerr << "Failed to close socket. Error: " << WSAGetLastError() << std::endl;
    return;
  }

  mIsListening = false;
}

NetworkService::EchoStatus NetworkService::echoSynchronously(long timeoutAfterSeconds) const {
  if (!mIsListening) {
    return EchoStatus::error;
  }

  constexpr size_t bufferSize = 65536;
  char inBuffer[bufferSize];

  sockaddr_in outAddress;
  int sizeOfOutAddress = sizeof(outAddress);

  timeval timeout;
  timeout.tv_sec = timeoutAfterSeconds;
  timeout.tv_usec = 0;

  fd_set fdSet;
  FD_ZERO(&fdSet);
  FD_SET(mSocket, &fdSet);

  // Wait for input
  int selectResult = select(0, &fdSet, 0, 0, &timeout);

  if (selectResult == -1) {
    std::cerr << "Error occured: " << WSAGetLastError() << std::endl;
    return EchoStatus::error;
  
  } else if (selectResult == 0) {
    return EchoStatus::timedout;
  }

  // Read input
  int inByteCount = recvfrom(mSocket, inBuffer, bufferSize, 0, (SOCKADDR*)&outAddress, &sizeOfOutAddress);

  if (inByteCount == SOCKET_ERROR) {
    std::cerr << "Recieving error: " << WSAGetLastError() << std::endl;
    return EchoStatus::error;
  }

  // Echo
  sendto(mSocket, inBuffer, inByteCount, 0, (SOCKADDR*)&outAddress, sizeOfOutAddress);
  return EchoStatus::success;
}

bool NetworkService::isListening() const {
  return mIsListening;
}

// Static

WSADATA NetworkService::wsaData = {};

bool NetworkService::initSystem() {
  if (WSAStartup(MAKEWORD(2, 2), &wsaData)) {
    std::cerr << "Failed to initialize WSA. Error: " << WSAGetLastError() << std::endl;
    return true;
  }

  return false;
}

void NetworkService::cleanSystem() {
  WSACleanup();
}
