#pragma once

#include <WinSock2.h>
#pragma comment(lib, "Ws2_32.lib")

class NetworkService {
// Types
public:  
  enum EchoStatus {
    success,
    error,
    timedout
  };

// Members
protected:
  SOCKET mSocket;
  sockaddr_in mInAddress;
  bool mIsListening;
  
public:
  NetworkService();
  ~NetworkService();

  bool listen(unsigned short port);
  void close();
  EchoStatus echoSynchronously(long timeoutAfterSeconds) const;
  bool isListening() const;

// Static
protected:
  static WSADATA wsaData;
public:
  static bool initSystem();
  static void cleanSystem();
};
